/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.utils;

import ohos.app.Context;

public class WindowUtil {
    /**
     * 1vp 约等于 160dpi 屏幕密度设备上的 1px
     *
     * @param context
     * @return 像素px
     */
    public static int getWindowWidth(Context context) {
        //返回的数值单位是vp*屏幕密度
        return context.getResourceManager().getDeviceCapability().width * context.getResourceManager().getDeviceCapability().screenDensity / 160;
    }

    /**
     * 1vp 约等于 160dpi 屏幕密度设备上的 1px
     *
     * @param context
     * @return px
     */
    public static int getWindowHeight(Context context) {
        //返回的数值单位是vp*屏幕密度
        return context.getResourceManager().getDeviceCapability().height * context.getResourceManager().getDeviceCapability().screenDensity / 160;
    }
}
