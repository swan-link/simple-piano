/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.utils;

import com.isoftstone.simplepiano.MainAbility;
import com.isoftstone.simplepiano.service.AudioServiceAbility;
import ohos.agp.utils.Color;

public class ConstantUtils {


    public static final int BASS_AREA = 0;           //低音区
    public static final int ALTO_SECTION = 1;        //中音区
    public static final int TREBLE = 2;              //高音区

    public static final int RANGE_ONE = 0;        //"大字二,一组"
    public static final int RANGE_TWO = 1;        //"大字组"
    public static final int RANGE_THREE = 2;      //"小字组"
    public static final int RANGE_FOUR = 3;       //"小字一组"
    public static final int RANGE_FIVE = 4;       //"小字二组"
    public static final int RANGE_SIX = 5;        //"小字三组"
    public static final int RANGE_SEVEN = 6;      //"小字四组,五组"

    //状态栏颜色
    public static final int COLOR_DEFAULT = Color.rgb(34, 34, 34);

    //音域选择结果
    public static final String RANGE_RESULT = "rangeResult";
    public static final String TRANSFERRED = "流转";
    public static final String IS_TRANSFERRED = "已流转";
    //FractionName
    public static final String ONE_FRACTION = "OneFraction";
    public static final String TWO_FRACTION = "TwoFraction";
    public static final String THREE_FRACTION = "ThreeFraction";
    public static final String FOUR_FRACTION = "FourFraction";
    public static final String FIVE_FRACTION = "FiveFraction";
    public static final String SIX_FRACTION = "SixFraction";
    public static final String SEVEN_FRACTION = "SevenFraction";


    public static final String AUDIO_ABILITY_MAIN = AudioServiceAbility.class.getName();
    public static final String ABILITY_MAIN = MainAbility.class.getName();

    public ConstantUtils() {
    }
}
