/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.dialog;

import com.isoftstone.simplepiano.ResourceTable;
import com.isoftstone.simplepiano.utils.ResourceTool;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;

public class LoadingDialog {

    private AnimatorProperty animatorProperty;
    private CommonDialog sDialog;
    private final Context mContext;

    public LoadingDialog(Context context) {
        this.mContext = context;
    }

    public void show() {
        if (sDialog != null) {
            sDialog.show();
        }
    }

    public void remove() {
        if (sDialog != null) {
            sDialog.destroy();
        }
    }

    public void create() {
        sDialog = new CommonDialog(mContext);
        sDialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        sDialog.setAlignment(LayoutAlignment.CENTER);
        sDialog.setOffset(0, 0);
        sDialog.setTransparent(true);
        sDialog.setContentCustomComponent(initDialog(sDialog));
        boolean mOutsideTouchClosable = false;
        sDialog.setAutoClosable(mOutsideTouchClosable);
    }

    //开启动画
    public void startAnimatorProperty() {
        if (animatorProperty != null && animatorProperty.isRunning()) {
            animatorProperty.stop();
        }

        if (animatorProperty != null && !animatorProperty.isRunning()) {
            if (!animatorProperty.isRunning()) {
                animatorProperty.start();
            }
        }
    }


    private Component initDialog(CommonDialog sDialog) {
        Component dialogLayout = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_loading_dialog, null, false);
        dialogLayout.setBackground(new ShapeElement() {{
            setCornerRadius(ResourceTool.getFloat(mContext, ResourceTable.Float_dialog_corner_radius, 0));
        }});
        Image image = (Image) dialogLayout.findComponentById(ResourceTable.Id_loading);
        animatorProperty = new AnimatorProperty();
        animatorProperty.setTarget(image);
        animatorProperty
                .rotate(360)
                .setLoopedCount(AnimatorValue.INFINITE)//无限循环
                .setCurveType(Animator.CurveType.BOUNCE); //反弹力效果

        if (sDialog != null) {
            sDialog.setDestroyedListener(() -> {
                if (animatorProperty.isRunning()) {
                    animatorProperty.stop();
                }
            });

        }
        return dialogLayout;
    }
}
