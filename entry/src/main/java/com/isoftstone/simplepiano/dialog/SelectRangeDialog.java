/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.dialog;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Picker;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;


public class SelectRangeDialog {
    private static final int DIALOG_WIDTH = 840;

    private static final int DIALOG_HEIGHT = 900;

    private ResultListener mResultListener;

    private CommonDialog commonDialog;

    private Picker rangePicker, groupPicker;

    //Picker内的显示内容
    int stringRegionIndex = 1;
    String[] stringRegion = {"低音区", "中音区", "高音区"};
    String[][] stringGroup = {{"大字二,一组", "大字组"}, {"小字组", "小组一组", "小字二组"}, {"小字三组", "小字四,五组"}};

    public SelectRangeDialog(Context context) {
        initView(context);
    }

    private void initView(Context context) {
        commonDialog = new CommonDialog(context);
        commonDialog.setAlignment(LayoutAlignment.CENTER);
        commonDialog.setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
        commonDialog.setAutoClosable(true);// 点击空白处自动关闭确认框
        Component dialogLayout =
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_range_dialog, null, false);
        commonDialog.setContentCustomComponent(dialogLayout);
        if (dialogLayout.findComponentById(ResourceTable.Id_picker_region) instanceof Picker) {
            rangePicker = (Picker) dialogLayout.findComponentById(ResourceTable.Id_picker_region);
            rangePicker.setWheelModeEnabled(false);
        }
        rangePicker.setMinValue(0);
        rangePicker.setMaxValue(stringRegion.length - 1);
        rangePicker.setFormatter(i -> stringRegion[i]);
        rangePicker.setValue(1);

        updateGroupPicker(dialogLayout);//设置初始的rangePicker和groupPicker的值
        //设置音区rangePicker的值更改监听器
        rangePicker.setValueChangedListener((picker, i, i1) -> {
            stringRegionIndex = i1;
            updateGroupPicker(dialogLayout);//rangePicker和groupPicker关联,当picker的值更改时,显示内容相对应
        });


        dialogLayout.findComponentById(ResourceTable.Id_cancel_btn).setClickedListener(component -> commonDialog.hide());
        Button affirmButton = (Button) dialogLayout.findComponentById(ResourceTable.Id_affirm_btn);
        affirmButton.setClickedListener(component -> {
                    mResultListener.callBack(rangePicker.getValue(), groupPicker.getValue());
                    commonDialog.hide();
                }
        );
    }

    private void updateGroupPicker(Component dialogLayout) {
        if (dialogLayout.findComponentById(ResourceTable.Id_picker_region) instanceof Picker) {
            groupPicker = (Picker) dialogLayout.findComponentById(ResourceTable.Id_picker_group);
            groupPicker.setWheelModeEnabled(false);
        }
        groupPicker.setMinValue(0);
        groupPicker.setMaxValue(stringGroup[stringRegionIndex].length - 1);
        groupPicker.setValue(1);
        groupPicker.setFormatter(i -> stringGroup[stringRegionIndex][i]);
    }

    //显示对话框
    public void show() {
        commonDialog.show();
    }


    //选择结果回调接口
    public interface ResultListener {
        void callBack(int regionValue, int groupValue);
    }

    public void setResultListener(ResultListener resultListener) {
        this.mResultListener = resultListener;
    }

    public void seTValue(int regionValue,int groupValue) {
        rangePicker.setValue(regionValue);
        groupPicker.setValue(groupValue);
    }

}
