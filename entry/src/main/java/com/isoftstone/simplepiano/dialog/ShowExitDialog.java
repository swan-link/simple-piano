/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.dialog;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;


public class ShowExitDialog {

    private static final int DIALOG_WIDTH = 840;

    private static final int DIALOG_HEIGHT = 300;

    private CommonDialog commonDialog;

    private ConfirmResultListener mConfirmResultListener;

    //退出流转对话框
    public ShowExitDialog(Context context) {
        initView(context);
    }

    private void initView(Context context) {
        commonDialog = new CommonDialog(context);
        commonDialog.setAlignment(LayoutAlignment.CENTER);
        commonDialog.setSize(DIALOG_WIDTH, DIALOG_HEIGHT);
        commonDialog.setAutoClosable(true);// 点击空白处自动关闭确认框

        Component dialogLayout =
                LayoutScatter.getInstance(context).parse(ResourceTable.Layout_exit_circulation_dialog, null, false);
        commonDialog.setContentCustomComponent(dialogLayout);
        dialogLayout.findComponentById(ResourceTable.Id_cancel).setClickedListener(component -> commonDialog.hide());
        Button affirmButton = (Button) dialogLayout.findComponentById(ResourceTable.Id_affirm);
        affirmButton.setClickedListener(component -> {
                    mConfirmResultListener.callBack();
                    commonDialog.hide();
                }
        );
    }

    //显示对话框
    public void show() {
        commonDialog.show();
    }

    public void setConfirmResultListener(ConfirmResultListener confirmresultListener) {
        this.mConfirmResultListener = confirmresultListener;
    }

    //选择设备结果回调接口
    public interface ConfirmResultListener {
        void callBack();
    }

}
