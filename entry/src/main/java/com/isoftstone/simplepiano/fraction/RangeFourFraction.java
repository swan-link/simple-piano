/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.fraction;

import com.isoftstone.simplepiano.ResourceTable;
import com.isoftstone.simplepiano.utils.ConstantUtils;
import ohos.agp.components.*;
import ohos.app.Context;

public class RangeFourFraction extends BaseFraction {
    private final Context mContext;

    public RangeFourFraction(Context context) {
        this.mContext = context;
    }

    @Override
    protected int getLayout() {
        return ResourceTable.Layout_range_four;
    }

    @Override
    public String fractionName() {
        return ConstantUtils.FOUR_FRACTION;
    }

    @Override
    protected void initView() {
        rangeSelectionUtils.startLocalAudioPlay(mContext);
        DirectionalLayout whiteFourLayout = (DirectionalLayout) getFractionAbility().findComponentById(ResourceTable.Id_four_whiteKey);
        rangeSelectionUtils.setFraction(this);
        rangeSelectionUtils.setContext(this);
        whiteFourLayout.setTouchEventListener(rangeSelectionUtils.getTouchEventListener());
        rangeSelectionUtils.initButton(ConstantUtils.FOUR_FRACTION);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

}
