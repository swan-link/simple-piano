/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.fraction;

import com.isoftstone.simplepiano.utils.RangeSelectionUtils;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

public abstract class BaseFraction extends Fraction {

    private Component mComponent;

    protected RangeSelectionUtils rangeSelectionUtils;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mComponent = scatter.parse(getLayout(), container, false);
        return mComponent;
    }

    protected abstract int getLayout();

    private void initUtils() {
        rangeSelectionUtils = new RangeSelectionUtils();
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        initUtils();
        initView();
    }

    public abstract String fractionName();

    protected abstract void initView();

}
