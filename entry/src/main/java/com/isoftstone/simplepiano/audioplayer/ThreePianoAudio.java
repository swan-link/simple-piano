/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class ThreePianoAudio {

    //声音播放器
    private final SoundPlayer soundThreePlayer;

    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerThreeMap;

    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;

    int[] threeAudio = {
            //小字组
            ResourceTable.Media_b_c_0, //c
            ResourceTable.Media_b_d_0, //d
            ResourceTable.Media_b_e_0, //e
            ResourceTable.Media_b_f_0, //f
            ResourceTable.Media_b_g_0, //g
            ResourceTable.Media_b_a_0, //a
            ResourceTable.Media_b_b_0, //b
            //小字组m
            ResourceTable.Media_h_c_0, //cm
            ResourceTable.Media_h_d_0, //dm
            ResourceTable.Media_h_f_0, //fm
            ResourceTable.Media_h_g_0, //gm
            ResourceTable.Media_h_a_0, //am
    };

    public ThreePianoAudio(Context context) {
        soundThreePlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerThreeMap = new HashMap<>();
        for (int i = 0; i < threeAudio.length; i++) {
            soundPlayerThreeMap.put(i, soundThreePlayer.createSound(context, threeAudio[i]));
        }
    }

    public void soundThreePlay(int number) {
        soundThreePlayer.play(soundPlayerThreeMap.get(number), soundPlayerParameters);
    }
}
