/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class SixPianoAudio {

    //声音播放器
    private final SoundPlayer soundSixPlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerSixMap;

    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;


    int[] sixAudio = {
            //小字三组
            ResourceTable.Media_b_c_3, //c3
            ResourceTable.Media_b_d_3, //d3
            ResourceTable.Media_b_e_3, //e3
            ResourceTable.Media_b_f_3, //f3
            ResourceTable.Media_b_g_3, //g3
            ResourceTable.Media_b_a_3, //a3
            ResourceTable.Media_b_b_3, //b3
            // 小字三组m
            ResourceTable.Media_h_c_3, //c3m
            ResourceTable.Media_h_d_3, //d3m
            ResourceTable.Media_h_f_3, //f3m
            ResourceTable.Media_h_g_3, //g3m
            ResourceTable.Media_h_a_3, //a3m
    };

    //钢琴音乐
    public SixPianoAudio(Context context) {
        soundSixPlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());

        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerSixMap = new HashMap<>();
        for (int i = 0; i < sixAudio.length; i++) {
            soundPlayerSixMap.put(i, soundSixPlayer.createSound(context, sixAudio[i]));
        }
    }

    public void soundSixPlay(int number) {
        soundSixPlayer.play(soundPlayerSixMap.get(number), soundPlayerParameters);
    }

}
