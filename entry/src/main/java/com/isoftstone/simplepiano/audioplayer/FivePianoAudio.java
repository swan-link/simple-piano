/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class FivePianoAudio {
    //声音播放器
    private final SoundPlayer soundFivePlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerFiveMap;
    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;
    int[] fiveAudio = {
            // 小字二组
            ResourceTable.Media_b_c_2, //c2
            ResourceTable.Media_b_d_2, //d2
            ResourceTable.Media_b_e_2, //e2
            ResourceTable.Media_b_f_2, //f2
            ResourceTable.Media_b_g_2, //g2
            ResourceTable.Media_b_a_2, //a2
            ResourceTable.Media_b_b_2, //b2

            // 小字二组m
            ResourceTable.Media_h_c_2, //c2m
            ResourceTable.Media_h_d_2, //d2m
            ResourceTable.Media_h_f_2, //f2m
            ResourceTable.Media_h_g_2, //g2m
            ResourceTable.Media_h_a_2, //a2m
    };

    public FivePianoAudio(Context context) {
        soundFivePlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerFiveMap = new HashMap<>();
        for (int i = 0; i < fiveAudio.length; i++) {
            soundPlayerFiveMap.put(i, soundFivePlayer.createSound(context, fiveAudio[i]));
        }
    }

    public void soundFivePlay(int number) {
        soundFivePlayer.play(soundPlayerFiveMap.get(number), soundPlayerParameters);
    }
}
