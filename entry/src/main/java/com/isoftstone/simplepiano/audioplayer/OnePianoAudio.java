/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class OnePianoAudio {

    //声音播放器
    private final SoundPlayer soundOnePlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerOneMap;
    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;

    int[] oneAudio = {
            //大字二组
            ResourceTable.Media_b_A2, //A2  0
            ResourceTable.Media_b_B2, //B2  1
            //大字一组
            ResourceTable.Media_b_C1, //C1  2
            ResourceTable.Media_b_D1, //D1  3
            ResourceTable.Media_b_E1, //E1  4
            ResourceTable.Media_b_F1, //F1  5
            ResourceTable.Media_b_G1, //G1  6
            ResourceTable.Media_b_A1, //A1  7
            ResourceTable.Media_b_B1, //B1  8
            //大字二组m
            ResourceTable.Media_h_A2, //A2m 9
            //大字一组m
            ResourceTable.Media_h_C1, //C1m 10
            ResourceTable.Media_h_D1, //D1m 11
            ResourceTable.Media_h_F1, //F1m 12
            ResourceTable.Media_h_G1, //G1m 13
            ResourceTable.Media_h_A1, //A1m 14
    };

    public OnePianoAudio(Context context) {
        soundOnePlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        //设置播放优先级 PLAY_IMMEDIATELY_PRIORITY为自动销毁  PLAY_DELAYED_PRIORITY为延迟销毁
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerOneMap = new HashMap<>();

        for (int i = 0; i < oneAudio.length; i++) {
            soundPlayerOneMap.put(i, soundOnePlayer.createSound(context, oneAudio[i]));
        }
    }

    public void soundOnePlay(int number) {
        soundOnePlayer.play(soundPlayerOneMap.get(number), soundPlayerParameters);
    }

}
