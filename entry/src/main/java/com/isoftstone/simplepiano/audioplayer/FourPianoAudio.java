/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class FourPianoAudio {
    //声音播放器
    private final SoundPlayer soundFourPlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerFourMap;
    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;
    int[] fourAudio = {
            //小字一组
            ResourceTable.Media_b_c_1, //c1
            ResourceTable.Media_b_d_1, //d1
            ResourceTable.Media_b_e_1, //e1
            ResourceTable.Media_b_f_1, //f1
            ResourceTable.Media_b_g_1, //g1
            ResourceTable.Media_b_a_1, //a1
            ResourceTable.Media_b_b_1, //b1
            //小字一组m
            ResourceTable.Media_h_c_1, //c1m
            ResourceTable.Media_h_d_1, //d1m
            ResourceTable.Media_h_f_1, //f1m
            ResourceTable.Media_h_g_1, //g1m
            ResourceTable.Media_h_a_1, //a1m
    };

    public FourPianoAudio(Context context) {
        soundFourPlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerFourMap = new HashMap<>();
        for (int i = 0; i < fourAudio.length; i++) {
            soundPlayerFourMap.put(i, soundFourPlayer.createSound(context, fourAudio[i]));
        }
    }

    public void soundFourPlay(int number) {
        soundFourPlayer.play(soundPlayerFourMap.get(number), soundPlayerParameters);
    }
}
