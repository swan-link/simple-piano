/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class TwoPianoAudio {

    //声音播放器
    private final SoundPlayer soundTwoPlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerTwoMap;
    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;
    int[] twoAudio = {
            //大字组
            ResourceTable.Media_b_C, //C    1
            ResourceTable.Media_b_D, //D    2
            ResourceTable.Media_b_E, //E    3
            ResourceTable.Media_b_F, //F    4
            ResourceTable.Media_b_G, //G    5
            ResourceTable.Media_b_A, //A    6
            ResourceTable.Media_b_B, //B    7
            //大字组m
            ResourceTable.Media_h_C, //Cm   8
            ResourceTable.Media_h_D, //Dm   9
            ResourceTable.Media_h_F, //Fm   10
            ResourceTable.Media_h_G, //Gm   11
            ResourceTable.Media_h_A, //Am   12
    };
    //钢琴音乐
    public TwoPianoAudio(Context context) {
        soundTwoPlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        //音频音量
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        //设置音量 0-100f
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        //设置播放次数
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        //设置播放速度
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        //设置播放优先级 PLAY_IMMEDIATELY_PRIORITY为自动销毁  PLAY_DELAYED_PRIORITY为延迟销毁
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        //音区
        soundPlayerTwoMap = new HashMap<>();
        for (int i = 0; i < twoAudio.length; i++) {
            soundPlayerTwoMap.put(i, soundTwoPlayer.createSound(context, twoAudio[i]));
        }
    }

    //绑定音乐与按键序号 播放音频
    public void soundTwoPlay(int number) {
        soundTwoPlayer.play(soundPlayerTwoMap.get(number), soundPlayerParameters);
    }

}
