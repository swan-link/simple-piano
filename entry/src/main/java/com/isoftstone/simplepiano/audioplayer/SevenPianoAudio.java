/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.audioplayer;

import com.isoftstone.simplepiano.ResourceTable;
import ohos.app.Context;
import ohos.media.audio.AudioManager;
import ohos.media.audio.SoundPlayer;

import java.util.HashMap;

public class SevenPianoAudio {

    //声音播放器
    private final SoundPlayer soundSevenPlayer;
    //声音播放器集合
    private final HashMap<Integer, Integer> soundPlayerSevenMap;
    //声音播放器参数
    private final SoundPlayer.SoundPlayerParameters soundPlayerParameters;

    int[] sevenAudio = {
            //小字四组
            ResourceTable.Media_b_c_4, //c4
            ResourceTable.Media_b_d_4, //d4
            ResourceTable.Media_b_e_4, //e4
            ResourceTable.Media_b_f_4, //f4
            ResourceTable.Media_b_g_4, //g4
            ResourceTable.Media_b_a_4, //a4
            ResourceTable.Media_b_b_4, //b4
            //小字五组
            ResourceTable.Media_b_c_5, //c5
            // 小字四组m
            ResourceTable.Media_h_c_4, //c4m
            ResourceTable.Media_h_d_4, //d4m
            ResourceTable.Media_h_f_4, //f4m
            ResourceTable.Media_h_g_4, //g4m
            ResourceTable.Media_h_a_4, //a4m
    };

    public SevenPianoAudio(Context context) {
        soundSevenPlayer = new SoundPlayer(AudioManager.AudioVolumeType.STREAM_MUSIC.getValue());
        soundPlayerParameters = new SoundPlayer.SoundPlayerParameters();
        SoundPlayer.AudioVolumes audioVolumes = new SoundPlayer.AudioVolumes();
        audioVolumes.setCentralVolume(100.0f);
        soundPlayerParameters.setVolumes(audioVolumes);
        soundPlayerParameters.setLoop(SoundPlayer.SoundPlayerParameters.NO_LOOP);
        soundPlayerParameters.setSpeed(SoundPlayer.SoundPlayerParameters.NORMAL_SPEED_RATE);
        soundPlayerParameters.setPriority(SoundPlayer.SoundPlayerParameters.PLAY_IMMEDIATELY_PRIORITY);
        soundPlayerSevenMap = new HashMap<>();

        for (int i = 0; i < sevenAudio.length; i++) {
            soundPlayerSevenMap.put(i, soundSevenPlayer.createSound(context, sevenAudio[i]));
        }
    }

    public void soundSevenPlay(int number) {
        soundSevenPlayer.play(soundPlayerSevenMap.get(number), soundPlayerParameters);
    }

}
