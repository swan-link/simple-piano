/*
 * Copyright (c) 2022 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.isoftstone.simplepiano.service;

import com.isoftstone.simplepiano.audioplayer.*;
import com.isoftstone.simplepiano.utils.ConstantUtils;
import com.isoftstone.simplepiano.utils.LogUtil;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.event.notification.NotificationRequest;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.rpc.IRemoteObject;

public class AudioServiceAbility extends Ability {

    private static final int NOTIFICATION_ID = 1005;
    private static final String TAG = AudioServiceAbility.class.getSimpleName();
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, TAG);

    private OnePianoAudio onePianoAudio;
    private TwoPianoAudio twoPianoAudio;
    private ThreePianoAudio threePianoAudio;
    private FourPianoAudio fourPianoAudio;
    private FivePianoAudio fivePianoAudio;
    private SixPianoAudio sixPianoAudio;
    private SevenPianoAudio sevenPianoAudio;
    public static final int PLAY_AUDIO_MSG = 100;

    @Override
    public void onStart(Intent intent) {
        HiLog.error(LABEL_LOG, "PlayerServiceAbility::onStart");
        super.onStart(intent);
        fourPianoAudio = new FourPianoAudio(getContext());
        onePianoAudio = new OnePianoAudio(getContext());
        twoPianoAudio = new TwoPianoAudio(getContext());
        threePianoAudio = new ThreePianoAudio(getContext());
        fivePianoAudio = new FivePianoAudio(getContext());
        sixPianoAudio = new SixPianoAudio(getContext());
        sevenPianoAudio = new SevenPianoAudio(getContext());

        NotificationRequest request = new NotificationRequest(NOTIFICATION_ID).setTapDismissed(true);
        NotificationRequest.NotificationNormalContent content = new NotificationRequest.NotificationNormalContent();
        content.setTitle("音频服务").setText("服务运行中...");
        NotificationRequest.NotificationContent notificationContent = new NotificationRequest.NotificationContent(content);
        request.setContent(notificationContent);
        keepBackgroundRunning(NOTIFICATION_ID, request);
    }


    @Override
    public void onStop() {
        super.onStop();
        HiLog.info(LABEL_LOG, "PlayerServiceAbility::onStop");
        //取消后台运行
        cancelBackgroundRunning();
    }

    @Override
    public IRemoteObject onConnect(Intent intent) {
        super.onConnect(intent);
        return new AudioRemountObject("AudioRemountObject").asObject();
    }

    @Override
    public void onDisconnect(Intent intent) {
        super.onDisconnect(intent);
    }

    //音频远程对象
    private class AudioRemountObject extends com.isoftstone.simplepiano.AudioPlaybackCapabilityInterfaceStub {
        public AudioRemountObject(String descriptor) {
            super(descriptor);
        }

        @Override
        public void sendCommand(int command, int soundId, int selectResults) {
            LogUtil.debug("AudioServiceAbility", "sendCommand");
            if (command == PLAY_AUDIO_MSG) {
                switch (selectResults) {
                    case ConstantUtils.RANGE_ONE:
                        onePianoAudio.soundOnePlay(soundId);
                        break;
                    case ConstantUtils.RANGE_TWO:
                        twoPianoAudio.soundTwoPlay(soundId);
                        break;
                    case ConstantUtils.RANGE_THREE:
                        threePianoAudio.soundThreePlay(soundId);
                        break;
                    case ConstantUtils.RANGE_FIVE:
                        fivePianoAudio.soundFivePlay(soundId);
                        break;
                    case ConstantUtils.RANGE_SIX:
                        sixPianoAudio.soundSixPlay(soundId);
                        break;
                    case ConstantUtils.RANGE_SEVEN:
                        sevenPianoAudio.soundSevenPlay(soundId);
                        break;
                    default:
                        fourPianoAudio.soundFourPlay(soundId);
                        break;
                }
            }
        }
    }
}