# 随身钢琴

HarmonyOS开发者创新大赛参赛作品

## 项目介绍

随身钢琴是一款HarmonyOS钢琴演奏APP，它能将手机变成一个虚拟的钢琴键盘进行乐曲演奏，它利用了了HarmonyOS的多屏流转特性，将钢琴键盘演奏区扩展到多个手机从而达到模拟真实88键钢琴的极佳体验。

## 功能截图

* APP首页

![img-1.png](doc/img-1.png)

* 音区选择

![img-2.png](doc/img-2.png)

* 多屏流转

利用HarmonyOS多屏流转特性，对钢琴键盘进行扩展

![img-3.png](doc/img-3.png)

* 乐曲演奏

支持多点触控和弦演奏

![img-4.png](doc/img-4.png)

## 团队介绍

我们是来自软通鸿湖万联的4位热爱HarmonyOS开发的小伙伴，有人擅长Java开发，有人擅长捕捉流行视觉元素，有人落地多个多端适配项目，也有人拥有12年开发经验，精通H5、FA、html、JS、VUE、Java、web后台等多项开发技术。

在此希望为HarmonyOS生态贡献我们的一份力量。我们团结一致，勇敢起航，信念和目标，永远洋溢在我们心中。

![img-5.png](doc/img-5.png)